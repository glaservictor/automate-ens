EXEC=pp
CFLAGS=-Wall  -ansi -std=gnu17 -Wpedantic -g
CC=gcc
SRC=$(wildcard *.c)
LIBS=$(wildcard *.h)
OBJ=$(patsubst %.c, obj/%.o, $(SRC))

all: $(EXEC)

$(EXEC): $(OBJ)
	$(CC) $(CFLAGS) $^ -o $(EXEC) $(LIBL)

obj/%.o : %.c
	$(CC) $(CFLAGS) -c -o $@ $< 

.PHONY: clean

clean:
	$(RM) $(OBJ)

mrproper: clean
	$(RM) $(EXEC)
