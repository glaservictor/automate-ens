#ifndef AUTOMATA_H
#define AUTOMATA_H

#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h> 
#include <locale.h>

#define STACK_SIZE 256
#define INDENT 3
#define EPS EOF

typedef enum Alphabet
{
  BLANK = 0,
  ACC_O = 1,
  ACC_C = 2,
  HASH = 3,
  SLASH = 4,
  STAR = 5,
  OTHER = 6,
  NEW_LINE = 7,
  SEMI = 8,
  QUOTE_SIMPLE = 9,
  QUOTE_DOUBLE = 10,
  END = 11,
  ASLASH= 12
} Alphabet;


typedef enum State
{
  LINE_BEG_WORD = 0,
  LINE_BEG_STACK = 1,
  DELETE_BLANK = 2,
  DINDENT = 3,
  ACC_OS = 4,
  ACC_CS = 5,
  IGNORE_LETTER = 6,
  IS_STAR = 7,
  IN_COMMENT = 8,
  CONTINUE_WORD = 9,
  CONTINUE_STACK = 10,
  IS_SLASH = 11,
  END_COMMENT = 12,
  ADD_COMMENT = 13,
  END_READ = 14,
  NEED_NL_STACK = 15,
  NEED_NL = 16,
  ACC_CS_NL = 17,
  NEED_NLC = 18,
  NEED_NLC_STACK = 19,
  IS_STAR_BEG = 20,
  DELETE_BLANK_COMMENT = 21,
  QUOTE_SIMPLE_STATE = 22,
  DELETE_BLANK_NL = 23,
  EMPTY_LINE = 24,
  ACC_CS_STACK = 25,
  QUOTE_DOUBLE_STATE = 26,
  IGNORE_NEXT_CHARS = 27,
  IGNORE_NEXT_CHARD = 28,
  IN_COMMENT_STACK = 29,
  SIMPLE_COMMENT = 30,
  FINAL
} State;


/**
 * @brief Triplé de retour d'une transition
 * -> nouvel état de l'automate 
 * -> lettre empilé 
 * -> booléen : 1 si on a lu la lettre en entrée 0 si il s'agit d'une EOFilon transition
 */
typedef struct Triple
{
  int auto_trans;
  char stack_trans;
  char read;
} triple;

/**
 * @brief Structure de l'automate
 */
typedef struct Automata{
  int initial;
  int final;
  char* stack;
  int size_stack;
}Automata;

/**
 * @brief Initialise l'automate 
 * -> Allocation de mémoire pour la pile
 * -> Définission des états
 * @return Automata* 
 */
Automata *init_automata();


/**
 * @brief Empile le carcatère c sur la pile
 * @param A 
 * @param c 
 */
void push_back_stack(Automata *A, char c);

/**
 * @brief Renvoie le dernier élément de la pile et le dépile
 * @param A 
 * @return int 
 */
int pop(Automata *A);


/**
 * @brief Ajoute l'indentation courante sur stdout
 * @param A
 */
void add_indent(Automata *A);

/**
 * @brief Lit le desssus de la pile
 * @param A 
 * @return int 
 */
int read_stack(Automata *A);

/**
 * @brief Convertit le caractère en drapeau entier
 * @param c 
 * @return Alphabet 
 */
Alphabet char_alpha(char c);

/**
 * @brief Fonction de transition de l'automate
 * @param A 
 * @param state 
 * @param input 
 * @return triple 
 */
triple transition_indent(Automata *A,int state, char input);


/**
 * @brief Fait tourner l'automate à partir de la chaîne en entrée
 * @param A 
 * @param str 
 */
void run_automata(Automata *A, char* str);

#endif
