#include "read_file.h"

#include "string.h"

char *read_file(int argc, char **argv)
{
  if (argc < 2)
    return NULL;
  FILE *file = fopen(argv[1], "r+");
  if (file == NULL)
    return NULL;
  int count = 0, CHAR_MAX = 1024, BUF_SIZE = 256, size_v = CHAR_MAX;
  char *txt = malloc(CHAR_MAX * sizeof(char));
  char *buf = malloc(BUF_SIZE * sizeof(char));
  size_t readable;
  while ((readable = fread(buf, sizeof(char), BUF_SIZE * sizeof(char), file)) > 0)
  {
    if (count + readable > size_v)
    {
      txt = realloc(txt, 2 * size_v * sizeof(char));
      size_v *= 2;
    }
    strcpy(txt + count, buf);
    count += readable;
  }
  txt[count] = EOF;
  txt[count + 1] = '\0';
  return txt;
}
