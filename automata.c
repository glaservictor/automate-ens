#include "automata.h"

int read_stack(Automata *A)
{
  return A->stack[A->size_stack - 1];
}

int pop(Automata *A)
{
  if (!A->size_stack)
    return EOF;
  int tmp = read_stack(A);
  A->size_stack--;
  return tmp;
}

Alphabet char_alpha(char c)
{
  switch (c)
  {
  case '{':
    return ACC_O;
  case '\n':
    return NEW_LINE;
  case '}':
    return ACC_C;
  case '#':
    return HASH;
  case '/':
    return SLASH;
  case '*':
    return STAR;
  case ';':
    return SEMI;
  case '\"':
    return QUOTE_DOUBLE;
  case '\'':
    return QUOTE_SIMPLE;
  case '\\':
    return ASLASH;
  case EOF:
    return END;
  default:
    return isblank(c) ? BLANK : OTHER;
  }
}

Automata *init_automata()
{
  Automata *A = malloc(sizeof(Automata));
  A->final = END_READ;
  A->initial = LINE_BEG_WORD;
  A->size_stack = 0;
  A->stack = malloc(STACK_SIZE * sizeof(char));
  return A;
}

void push_back_stack(Automata *A, char c) { A->stack[A->size_stack++] = c; }

void add_indent(Automata *A)
{
  int indent = read_stack(A);
  while (indent > 0)
  {
    putchar(' ');
    indent--;
  }
}

triple transition_indent(Automata *A, int state, char input)
{
  int unread = state == LINE_BEG_STACK || state == CONTINUE_STACK;

  //On va limiter la duplication de code en gérant la lecture sur la pile et sur l'entrée de manière analogue
  //En prenant en compte le fait que lire sur la pile est équivalent à ne pas "avancer" sur la chaîne d'entrée

  if (unread)
    input = pop(A);
  int cast_input = char_alpha(input), indent, read;
  //On cast notre caractère en drapeau

  switch (state)

  //disjonction sur l'état de notre automate

  {
  case LINE_BEG_WORD:
  case CONTINUE_WORD:
  case LINE_BEG_STACK:
  case CONTINUE_STACK:
    //On traite le cas "de base" quand on lit depuis le mot en regroupant
    //la lecture sur la pile ou sur le mot en entré

    switch (cast_input)

    //disjonction sur le caractère lu

    {
    case END:
      return (triple){END_READ, EOF, 0};
    case NEW_LINE:
      putchar(input);
      if (state == LINE_BEG_WORD || state == LINE_BEG_STACK)
        return (triple){EMPTY_LINE, EOF, !unread};
      return (triple){LINE_BEG_WORD, EOF, !unread};

      // Ici si on a un caractère de fin de ligne en début de ligne il s'agit d'une ligne vide
      // On passe alors dans l'état EMPTY_LINE
      // Si on était en milieu de ligne on passe à la ligne on laisse systématiquement une ligne vide si on en a n vide ( comme clangdformat )

    case BLANK:
      if (state == CONTINUE_WORD || state == CONTINUE_STACK)
      {
        putchar(' ');
        return (triple){DELETE_BLANK, EOF, !unread};
      }
      return (triple){DELETE_BLANK_NL, EOF, !unread};

      // On supprime les caractères blancs sauf un
      // Si on est en début de ligne on le notifie pour ne pas conserver un espace blanc

    case ACC_O:
      if (state == CONTINUE_WORD || state == CONTINUE_STACK)
        putchar('\n');
      add_indent(A);
      putchar(input);
      return (triple){ACC_OS, EOF, !unread};
      break;

      //On ajoute l'indentation courante et on rentre dans l'état accolade ouverte
      //On va a la ligne si on est en milieu de ligne

    case ACC_C:
      if (state == CONTINUE_WORD || state == CONTINUE_STACK)
        putchar('\n');
      return (triple){ACC_CS, EOF, !unread};
      break;

    case HASH:
      putchar(input);
      return (triple){IGNORE_LETTER, EOF, !unread};

      //On ignore la chaîne jusqu'à un retour chariot

    case SEMI:
      if (state == LINE_BEG_WORD || state == LINE_BEG_STACK)
        add_indent(A);
      putchar(input);
      return (triple){NEED_NL, EOF, !unread};

      //En début de ligne on ajoute l'indentation sinon on l'affiche

    case QUOTE_SIMPLE:
      putchar(input);
      return (triple){QUOTE_SIMPLE_STATE, EOF, !unread};

    case QUOTE_DOUBLE:
      putchar(input);
      return (triple){QUOTE_DOUBLE_STATE, EOF, !unread};

    case SLASH:
      return (state == LINE_BEG_WORD || state == LINE_BEG_STACK) ? (triple){IS_STAR_BEG, EOF, !unread} : (triple){IS_STAR, EOF, !unread};

      //Ici on va regarder si il s'agit du début d'un commentaire i.e si une étoile est rencontré après

    default:
      if (state == LINE_BEG_WORD || state == LINE_BEG_STACK)
        add_indent(A);
      putchar(input);
      return (triple){CONTINUE_WORD, EOF, !unread};

      // Par défout on continue à lire en rajoutant si nécessaire l'indentation courante
    };
    break;

  case QUOTE_SIMPLE_STATE:
    if (cast_input == END)
    {
      putchar('\n');
      fprintf(stderr, "Citation non fermée\n");
      return (triple){END_READ, EOF, 0};
    }
    putchar(input);
    if (cast_input == ASLASH)
      return (triple){IGNORE_NEXT_CHARS, EOF, 1};
    else if (cast_input == QUOTE_SIMPLE)
      return (triple){CONTINUE_WORD, EOF, 1};
    else
      return (triple){QUOTE_SIMPLE_STATE, EOF, 1};

    // Il s'agit d'ignorer comme pour la version QUOTE_DOUBLE les caractères dans une chaîne
    // A noter qu'on vérifie qu'on ne prend pas \' pour une fin de citation
    // On rentre dans l'état final si on a en entrée un EOF

  case QUOTE_DOUBLE_STATE:
    if (cast_input == END)
    {
      putchar('\n');
      fprintf(stderr, "Citation non fermée\n");
      return (triple){END_READ, EOF, 0};
    }
    putchar(input);
    if (cast_input == ASLASH)
      return (triple){IGNORE_NEXT_CHARD, EOF, 1};
    else if (cast_input == QUOTE_DOUBLE)
      return (triple){CONTINUE_WORD, EOF, 1};
    else
      return (triple){QUOTE_DOUBLE_STATE, EOF, 1};

  case IGNORE_NEXT_CHARD:
    putchar(input);
    return (triple){QUOTE_DOUBLE_STATE, EOF, 1};

  case IGNORE_NEXT_CHARS:
    putchar(input);
    return (triple){QUOTE_SIMPLE_STATE, EOF, 1};

    //On affiche sans formatter le texte reçu tant qu'il ne s'agit pas d'un guillemet

  case ACC_OS:
    indent = pop(A);
    if (cast_input == NEW_LINE)
    {
      putchar(input);
      return (triple){LINE_BEG_WORD, indent + INDENT, 1};
    }
    push_back_stack(A, indent + INDENT);
    if (cast_input == END)
      return (triple){END_READ, EOF, 0};
    return (triple){NEED_NL_STACK, input, 1};

    // On récupère l'indentation courante et on l'empile additionné à la variable INDENT défini dans automate.h
    // Sans oublier la gestion du caractère EOF

  case NEED_NL_STACK:
  case NEED_NL:
    read = state == NEED_NL;
    if (!read)
    {
      input = pop(A);
      cast_input = char_alpha(input);
    }
    if (cast_input == NEW_LINE)
    {
      putchar(input);
      return (triple){LINE_BEG_WORD, EOF, read};
    }
    if (cast_input == BLANK)
      return (triple){NEED_NL, EOF, read};
    if (cast_input == END)
      return (triple){END_READ, EOF, 0};
    putchar('\n');
    return (triple){LINE_BEG_STACK, input, read};

    // Ces deux états permettent de finir une ligne correctement
    // On gère simplement le cas où on lit depuis la pile pour ne pas lire le caractère courant

  case DELETE_BLANK_NL:
  case DELETE_BLANK:
    switch (cast_input)
    {
    case BLANK:
      return state == DELETE_BLANK ? (triple){DELETE_BLANK, EOF, 1} : (triple){DELETE_BLANK_NL, EOF, 1};
    case END:
      return (triple){END_READ, EOF, 0};
    default:
      state = (state == DELETE_BLANK_NL ? LINE_BEG_STACK : CONTINUE_STACK);
      return (triple){state, input, 1};
    }
    break;

    //Délétion des caractères blancs

  case ACC_CS_STACK:
  case ACC_CS:
    indent = pop(A);
    if (indent >= INDENT)
      indent -= INDENT;
    else
    {
      fprintf(stderr, "Erreur dans la fermeture d'accolade\n");
      indent = 0;
    }
    push_back_stack(A, indent);
    add_indent(A);
    putchar('}');
    return (triple){NEED_NL_STACK, input, 1};

    //Gestion de la fermeture d'accolade

  case IS_STAR_BEG:
  case IS_STAR:
    if (cast_input == SLASH)
    {
      printf("//");
      return (triple){SIMPLE_COMMENT, EOF, 1};
    }
    if (cast_input == STAR)
    {
      if (state == IS_STAR)
        putchar('\n');
      add_indent(A);
      printf("/*");
      fflush(stdout);
      return (triple){IN_COMMENT, EOF, 1};
    }
    putchar(input);
    return (triple){CONTINUE_WORD, EOF, 1};

    // On regarde si on entre dans un commentaire donc si le slash est suivi d'une étoile ou d'un slash

  case SIMPLE_COMMENT:
    putchar(input);
    if (cast_input == NEW_LINE)
      return (triple){LINE_BEG_WORD, EOF, 1};
    return (triple){SIMPLE_COMMENT, EOF, 1};

    // Commentaire classique ( double slash )

  case IN_COMMENT_STACK:
    input = pop(A);
    cast_input = char_alpha(input);
  case IN_COMMENT:
    if (cast_input == STAR)
    {
      putchar(input);
      return (triple){END_COMMENT, EOF, 1};
    }
    if (cast_input == NEW_LINE)
    {
      printf("**/\n");
      return (triple){ADD_COMMENT, EOF, 1};
    }
    if (cast_input == END)
    {
      putchar('\n');
      fprintf(stderr, "Commentaire non fermé\n");
      return (triple){END_READ, EOF, 0};
    }
    putchar(input);
    return (triple){IN_COMMENT, EOF, 1};

    // Gestion du commentaire étoilé i.e si on arrive à une fin de ligne ou si on rencontre une étoile

  case ADD_COMMENT:
    if (cast_input == END )
      return (triple){END_READ, EOF, 0};
    add_indent(A);
    printf("/*");
    if (cast_input == BLANK)
      return (triple){DELETE_BLANK_COMMENT, EOF, 1};
    putchar(input);
    return (triple){IN_COMMENT, EOF, 1};

    // Ajout d'un commentaire après un saut de ligne

  case DELETE_BLANK_COMMENT:
    if (cast_input == BLANK)
      return (triple){DELETE_BLANK_COMMENT, EOF, 1};
    if (cast_input == END)
      return (triple){END_READ, EOF, 0};
    putchar(' ');
    return (triple){IN_COMMENT_STACK, input, 1};
    break;

    // Suppression blanc avant un commentaire ( pour n'afficher que l'indentation sans espaces supplémentaires )

  case END_COMMENT:
    putchar(input);
    if (cast_input == SLASH)
      return (triple){NEED_NL, EOF, 1};
    if (cast_input == STAR)
      return (triple){END_COMMENT, EOF, 1};
    if (cast_input == END)
      return (triple){END_READ, EOF, 0};
    return (triple){IN_COMMENT, EOF, 1};

    // Vérification de fin de commentaire

  case IGNORE_LETTER:
    if (cast_input == END)
      return (triple){END_READ, EOF, 0};
    putchar(input);
    if (cast_input == NEW_LINE)
      return (triple){LINE_BEG_WORD, EOF, 1};
    return (triple){IGNORE_LETTER, EOF, 1};

  // On skip les lettres pour les instructuions aux préprocesseur 

  case EMPTY_LINE:
    if (cast_input == NEW_LINE || cast_input == BLANK)
      return (triple){EMPTY_LINE, EOF, 1};
    if (cast_input == END)
      return (triple){END_READ, EOF, 0};
    return (triple){LINE_BEG_STACK, input, 1};

  case END_READ:
    indent = read_stack(A);
    int n = indent / INDENT;
    if (indent > 0)
      fprintf(stderr, "Erreur(s) d'indentation : %d\n", n);
    return (triple){FINAL, EOF, 0};
  }
    //On vérifie les erreurs d'indentation finales en regardant la différence entre
    //l'indentation courante et la taille d'une tabulation

  return (triple){LINE_BEG_WORD, EOF, 1};
}

void run_automata(Automata *A, char *str)
{
  push_back_stack(A, 0);
  size_t incr = 0;
  triple result = {A->initial, EOF, 1};
  while (incr < strlen(str) && result.stack_trans != FINAL)
  {
    result = transition_indent(A, result.auto_trans, str[incr]);
    if (result.stack_trans != EOF)
      push_back_stack(A, result.stack_trans);
    if (result.read)
      incr++;
  }
  putchar('\n');
  return;
}
