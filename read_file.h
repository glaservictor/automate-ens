#include "string.h"
#include <stdio.h>
#include <stdlib.h>


/**
 * @brief Fonction de lecture de fichier 
 * @param int argc
 * @param char** argv
 * @return char* 
 */
char *read_file(int, char **);
