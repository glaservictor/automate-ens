#include <stdlib.h>
#include "automata.h"
#include "read_file.h"

int main(int argc, char **argv)
{
    Automata *A = init_automata();
    char *txt = read_file(argc, argv);
    if (txt == NULL)
        return -1;
    run_automata(A, txt);
    return 0;
}