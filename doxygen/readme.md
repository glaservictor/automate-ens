# PRETTY PRINTER 
## Projet C | PROG S1 

Automate à pile pour correction syntaxique de fichier C.

### 1. Squelette du code
##### auomate.c :
   * Fonction de transition de l'automate 
        * transition_indent
   * Execution globale de l'automate 
        * run_automata
   * Fonction annexes 
        * pop
        * read_stack
        * char_alpha
        * push_back_stack
        * add_indent
        * init_automata
        
##### read_file.c :
   Transformation du fichier en argument en chaîne de caractères
##### pp.c :
   Fonction principale

### 2. Forme de l'automate

* __Interêts__ : Ici on a choisi l'utilisation d'un automate à pile pour deux raisons. 
La première est que cela permet de stocker l'indentation courante sur le sommet de la pile. La seconde est que pour vérifier la bonne terminaison d'une fin de ligne on doit lire les lettres depuis l'état NEED_NL dans ce code. En effet à l'issue d'une accolade fermante, on doit sauter au plus une ligne et supprimer tous les caractères blancs. De ce fait en lisant un caractère non blanc on doit le stocker pour le lire depuis l'état NEW_LINE et non NEED_NL.

* __Désavantages__ : L'automate n'est à priori pas représentable. De ce fait ajouter n espaces ne peut se faire pour un nombre d'espaces non borné. De se fait on a pris certaine liberté de ce point de vu. 

* __Libertés__ : Comme dit précedemment on s'est permis d'ajouter des espaces en sortant du cadre de l'automate à pile. Certaines parties du code font "plus" que ce que pourrait faire un automate pour condenser le code mais pourrait être exécuter en ajoutant plus d'états. Enfin, même si cela n'est pas recommandé on s'est autorisé d'utiliser des étiquettes sur certaines parties du code pour simuler une EOFilon transition. Ici on a déliberemment pas mis les citations au niveau de l'indentation pour rester proche de ce qui est cohérent en écriture C/C++.
  

### 3. Fonctionnalité supplémantaire
Certaines fonctionnalitées ont été implémentés en plus de ce qui était demandé. En effet la gestion des caractères spéciaux dans les citations, des commentaires en double slash mais l'essentiel du travail s'est concentré sur mimer au mieux les comportements exotique clangdformat. C'est ce qui m'a notamment inspiré pour la gestion des caractères blancs ou les lignes vides. 

####Autor
__Victor Glaser__


![](http://lle.ens-lyon.fr/logo-1.png/?lang=fr)

#### 
