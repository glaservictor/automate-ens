/*
@licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2019 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of version 2 of the GNU General Public License as published by
the Free Software Foundation

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Pretty-Printer", "index.html", [
    [ "PRETTY PRINTER", "index.html", [
      [ "Projet C | PROG S1", "index.html#autotoc_md11", [
        [ "1. Squelette du code", "index.html#autotoc_md12", null ],
        [ "2. Forme de l'automate", "index.html#autotoc_md16", null ],
        [ "3. Fonctionnalité supplémantaire", "index.html#autotoc_md17", [
          [ "Autor", "index.html#autotoc_md18", null ],
          [ "autotoc_md19", "index.html#autotoc_md19", null ]
        ] ]
      ] ]
    ] ],
    [ "PRETTY PRINTER", "d3/dcc/md__r_e_a_d_m_e.html", [
      [ "Projet C | PROG S1", "d3/dcc/md__r_e_a_d_m_e.html#autotoc_md1", [
        [ "1. Documentation", "d3/dcc/md__r_e_a_d_m_e.html#autotoc_md2", null ],
        [ "2. Squelette du code", "d3/dcc/md__r_e_a_d_m_e.html#autotoc_md3", null ],
        [ "3. Forme de l'automate", "d3/dcc/md__r_e_a_d_m_e.html#autotoc_md7", null ],
        [ "4. Fonctionnalité supplémantaire", "d3/dcc/md__r_e_a_d_m_e.html#autotoc_md8", [
          [ "autotoc_md9", "d3/dcc/md__r_e_a_d_m_e.html#autotoc_md9", null ]
        ] ]
      ] ]
    ] ],
    [ "Structures de données", "annotated.html", [
      [ "Structures de données", "annotated.html", "annotated_dup" ],
      [ "Index des structures de données", "classes.html", null ],
      [ "Champs de donnée", "functions.html", [
        [ "Tout", "functions.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Fichiers", "files.html", [
      [ "Liste des fichiers", "files.html", "files_dup" ],
      [ "Variables globale", "globals.html", [
        [ "Tout", "globals.html", null ],
        [ "Fonctions", "globals_func.html", null ],
        [ "Définitions de type", "globals_type.html", null ],
        [ "Énumérations", "globals_enum.html", null ],
        [ "Valeurs énumérées", "globals_eval.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"annotated.html"
];

var SYNCONMSG = 'cliquez pour désactiver la synchronisation du panel';
var SYNCOFFMSG = 'cliquez pour activer la synchronisation du panel';