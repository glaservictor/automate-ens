var searchData=
[
  ['ignore_5fletter_29',['IGNORE_LETTER',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8ae501130bf981758fd58fe23c5e140d27',1,'automata.h']]],
  ['ignore_5fnext_5fchard_30',['IGNORE_NEXT_CHARD',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8ac3ba16eee4d3d6d97c54b5893aaf01f5',1,'automata.h']]],
  ['ignore_5fnext_5fchars_31',['IGNORE_NEXT_CHARS',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a825ce2eab77f43f1f1bc8f0d783e2d0b',1,'automata.h']]],
  ['in_5fcomment_32',['IN_COMMENT',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a9533085927f677c97351684c2dc6c826',1,'automata.h']]],
  ['in_5fcomment_5fstack_33',['IN_COMMENT_STACK',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a3b65e97dc598e36b5ade13582c2971d6',1,'automata.h']]],
  ['indent_34',['INDENT',['../d3/d5f/automata_8h.html#a502b06aa5ad25116c775d201326bad52',1,'automata.h']]],
  ['init_5fautomata_35',['init_automata',['../d1/d63/automata_8c.html#a62e7b5d235f5f1fdc25b6f335d78287c',1,'init_automata():&#160;automata.c'],['../d3/d5f/automata_8h.html#a62e7b5d235f5f1fdc25b6f335d78287c',1,'init_automata():&#160;automata.c']]],
  ['initial_36',['initial',['../dc/dcb/struct_automata.html#aea6d06dcaf74fd77d6ea8936fccbd9e4',1,'Automata']]],
  ['is_5fslash_37',['IS_SLASH',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a1892d33fbc771ffa1690a2f8bfe460ea',1,'automata.h']]],
  ['is_5fstar_38',['IS_STAR',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a3ac004baa3de37dab5483a7b7defdf7f',1,'automata.h']]],
  ['is_5fstar_5fbeg_39',['IS_STAR_BEG',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a76bb5bffa3162588bccc140c383c7c0a',1,'automata.h']]]
];
