var searchData=
[
  ['semi_65',['SEMI',['../d3/d5f/automata_8h.html#ad321c418983642eb2b4360826d489ec4a68cf729acfb0cf4857abec621022e0df',1,'automata.h']]],
  ['simple_5fcomment_66',['SIMPLE_COMMENT',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8a387d534d03da1c5d8328a5aa625d8bc5',1,'automata.h']]],
  ['size_5fstack_67',['size_stack',['../dc/dcb/struct_automata.html#a27425651cb9e1a80742c041728a97c0d',1,'Automata']]],
  ['slash_68',['SLASH',['../d3/d5f/automata_8h.html#ad321c418983642eb2b4360826d489ec4aaabdfe7000ca535236502bb0c87ad944',1,'automata.h']]],
  ['stack_69',['stack',['../dc/dcb/struct_automata.html#aa824415a13ea9db9ce8a349fa9ad31b3',1,'Automata']]],
  ['stack_5fsize_70',['STACK_SIZE',['../d3/d5f/automata_8h.html#a6423a880df59733d2d9b509c7718d3a9',1,'automata.h']]],
  ['stack_5ftrans_71',['stack_trans',['../dd/da5/struct_triple.html#a1f62d4d86422de84fca7beb9c51830b2',1,'Triple']]],
  ['star_72',['STAR',['../d3/d5f/automata_8h.html#ad321c418983642eb2b4360826d489ec4a9d398750ed310ae69cd070016810e4dc',1,'automata.h']]],
  ['state_73',['State',['../d3/d5f/automata_8h.html#a5d74787dedbc4e11c1ab15bf487e61f8',1,'State():&#160;automata.h'],['../d3/d5f/automata_8h.html#a2b290a57d2211585af93443ed7cf5fa3',1,'State():&#160;automata.h']]]
];
